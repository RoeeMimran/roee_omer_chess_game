#pragma once

#include "Board.h"
#include "Communication.h"

#define Y 1
#define X 0

class ChessManager
{
private:
	Board* _board;
	Pipe _pipe;
	void initializedChess();

public:
	void chess();
};