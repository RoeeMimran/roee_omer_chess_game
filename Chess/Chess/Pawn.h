﻿#pragma once
#include "GamePiece.h"
#include "Board.h"

class Board;
class GamePiece;

#define UP 1
#define DOWN -1 

class Pawn : public GamePiece//фаеп
{
public:
	Pawn(const int x, const int y, const char id);
	~Pawn();
	void checkMove(Board& board, const int newX, const int newY, bool isWhite);
	void virtual chessWillOccure(GamePiece* temp, Board& board, int kingX, int kingY, bool isWhite);

private:
	bool _firstMove;
};
