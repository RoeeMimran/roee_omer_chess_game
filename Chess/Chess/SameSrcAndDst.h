#pragma once

#include "Exceptions.h"

class SameSrcAndDst : public std::exception
{
public:
	const char* what() const;
};
