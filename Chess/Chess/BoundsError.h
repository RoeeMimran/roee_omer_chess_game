#pragma once
#include "Exceptions.h"

class BoundsError : public std::exception
{
public:
	const char* what() const;
};
