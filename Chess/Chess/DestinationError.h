#pragma once
#include "Exceptions.h"

class DestinationError : public std::exception
{
public:
	const char* what() const;
};
