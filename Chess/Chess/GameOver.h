#pragma once
#include "Exceptions.h"

class GameOver : public std::exception
{
public:
	const char* what() const;
};
