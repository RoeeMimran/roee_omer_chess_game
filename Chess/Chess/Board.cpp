#include "Board.h"

/*
c'tor:
function create the board of the game, all the game pieces
input: none
output: none
*/
Board::Board() : _currBoard()
{
	int row = 0;
	int col = 0;
	char currPiece = 'a';

	for (col = 0; col < NUM_OF_COL; col++)
	{
		for (row = 0; row < NUM_OF_ROW; row++)
		{
			// get the current piece from the string of the start game
			currPiece = START_OF_GAME[col * NUM_OF_COL + row];

			// update the id of the piece and create it
			this->_currBoard[col][row] = this->createPiece(currPiece, row, col);
		}
	}
}

/*
d'tor:
free all the dynamic location we located for the game pieces
input: none
output: none
*/
Board::~Board()
{
	int col = 0;
	int row = 0;

	for (col = 0; col < NUM_OF_COL; col++)
	{
		for (row = 0; row < NUM_OF_ROW; row++)
		{
			delete this->_currBoard[col][row];
		}
	}
}

/*
function create piece dynamicly
input: what piece do we want to create and his place on the board
output: none
*/
GamePiece* Board::createPiece(char pieceToCreate, const int x, const int y)
{
	// create the wanted piece in the wanted place
	GamePiece* newPiece = 0;

	if (pieceToCreate == WHITE_KING || pieceToCreate == BLACK_KING)
	{
		newPiece = new King(x, y, pieceToCreate);
	}
	else if (pieceToCreate == WHITE_QUEEN || pieceToCreate == BLACK_QUEEN)
	{
		newPiece = new Queen(x, y, pieceToCreate);
	}
	else if (pieceToCreate == WHITE_BISHOP || pieceToCreate == BLACK_BISHOP)
	{
		newPiece = new Bishops(x, y, pieceToCreate);
	}
	else if (pieceToCreate == WHITE_KNIGHT || pieceToCreate == BLACK_KNIGHT)
	{
		newPiece = new Knight(x, y, pieceToCreate);
	}
	else if (pieceToCreate == WHITE_ROOK || pieceToCreate == BLACK_ROOK)
	{
		newPiece = new Rook(x, y, pieceToCreate);
	}
	else if (pieceToCreate == WHITE_PAWN || pieceToCreate == BLACK_PAWN)
	{
		newPiece = new Pawn(x, y, pieceToCreate);
	}
	else
	{
		// empty place
		newPiece = 0;
	}
	return newPiece;
}

/*
function print the board
input: none
output: none
*/
void Board::printBoard() const
{
	int row = 0;
	int col = 0;
	for (col = 0; col < NUM_OF_COL; col++)
	{
		for (row = 0; row < NUM_OF_ROW; row++)
		{
			if (this->_currBoard[col][row])
			{
				// print the id
				std::cout << this->_currBoard[col][row]->getId() << "  ";
			}
			else
			{
				// empty place
				std::cout << "#  ";
			}
		}
		std::cout << std::endl;
	}
}

/*
function update the board after all the checks
input: the old place of the piece and the new place
output: none
*/
void Board::updateBoard(const int oldX, const int oldY, const int newX, const int newY)
{
	this->_currBoard[newY][newX] = this->_currBoard[oldY][oldX];
	this->_currBoard[oldY][oldX] = 0;
}
