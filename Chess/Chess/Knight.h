#pragma once
#include "GamePiece.h"
#include "Board.h"

class Board;
class GamePiece;

class Knight : public GamePiece//���
{
public:
	Knight(const int x, const int y, const char id);
	~Knight();
	void checkMove(Board& board, const int newX, const int newY, bool isWhite);
};