#include "ChessManager.h"

/*
function initialized the game, the board and the pipe
input: none
output: none
*/
void ChessManager::initializedChess()
{
	this->_board = new Board;
	Communication::connect(this->_pipe);
	Communication::sendData(START_OF_GAME, this->_pipe);
}

/*
the main function, the chess game
input: none
output: none
*/
void ChessManager::chess()
{
	bool gameOver = false;
	bool isWhite = true;
	int inputPlace[2] = { 0 }; // x, y
	int newPlace[2] = { 0 }; // x, y
	std::string dataFromGUI = "";
	GamePiece* currPiece = 0;

	try
	{
		this->initializedChess();
	}
	catch (Communication)
	{
		// the player doesnt want to keep connecting
		gameOver = true;
	}

	while (!gameOver)
	{
		dataFromGUI = Communication::getData(this->_pipe);

		// data from gui to number that we will be able to put it in the array
		inputPlace[X] = Communication::dataToIndex(dataFromGUI[X]);
		inputPlace[Y] = Communication::dataToIndex(dataFromGUI[Y]);
		newPlace[X] = Communication::dataToIndex(dataFromGUI[X + 2]);
		newPlace[Y] = Communication::dataToIndex(dataFromGUI[Y + 2]);

		currPiece = this->_board->_currBoard[inputPlace[Y]][inputPlace[X]];

		try
		{
			if (dataFromGUI == "quit")
			{
				// checking if the user quit
				throw dataFromGUI;
			}
			else if (!currPiece)
			{
				throw NotYourPlayer();
			}

			// move the piece and send the GUI that the move illegal
			currPiece->move(*this->_board, newPlace[X], newPlace[Y], isWhite);
			Communication::sendData("0", this->_pipe);

			// change the turn to the other player
			isWhite = !isWhite;
		}
		catch (const std::exception& e)
		{
			// send the error that happened
			Communication::sendData(e.what(), this->_pipe);

			if (e.what()[0] == '1')
			{
				// change the turn
				isWhite = !isWhite;
			}
		}
		catch (const std::string &e)
		{
			// the user quited
			gameOver = true;
			std::cout << e << std::endl;
		}
	}
}
