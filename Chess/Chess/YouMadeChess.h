#pragma once
#include "Exceptions.h"

class YouMadeChess : public std::exception
{
public:
	const char* what() const;
};
