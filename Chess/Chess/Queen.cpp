#include "Queen.h"
Queen::Queen(const int x, const int y, const char id) :GamePiece(x, y, id)
{

}

/*
queen is a combination of bishop and rook so the checks are the same
input: current board and the place the queen want to go to, and which turn
output: none
*/
void Queen::checkMove(Board& board, const int newX, const int newY, bool isWhite)
{
	try 
	{
		// we need that only one of them will be good
		checkRook(board, this->getX(), this->getY(), newX, newY);
	}
	catch (std::exception)
	{
		checkBishop(board, this->getX(), this->getY(), newX, newY);
	}

	sameSrcAndDst(newX, newY);
	notYourPlayer(board, isWhite);
	samePlayerPieceAtDst(board, newX, newY);
}