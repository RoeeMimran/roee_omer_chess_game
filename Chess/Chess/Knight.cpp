#include "Knight.h"

Knight::Knight(const int x, const int y, const char id) :GamePiece(x, y, id)
{

}
Knight::~Knight()
{

}
/*
input: Board& board,int newX,int newY,bool isWhite
output none
the function will check all the avalible moves that the piece can make and other tests like same source and destenation
not your player,chess will accure and same player at destenation
*/
void Knight::checkMove(Board& board, const int newX, const int newY, bool isWhite)
{
	if (!(
		(this->getX() + 2 == newX && this->getY() + 1 == newY) ||
		(this->getX() + 1 == newX && this->getY() + 2 == newY) ||
		(this->getX() - 2 == newX && this->getY() - 1 == newY) ||
		(this->getX() - 1 == newX && this->getY() - 2 == newY) ||
		(this->getX() - 1 == newX && this->getY() + 2 == newY) ||
		(this->getX() - 2 == newX && this->getY() + 1 == newY) ||
		(this->getX() + 2 == newX && this->getY() - 1 == newY) ||
		(this->getX() + 1 == newX && this->getY() - 2 == newY)))
	{
		throw IllegalMovement();
	}
	if (newX < 0 || newX >8 || newY < 0 || newY>8)//outside the map
	{
		throw BoundsError();
	}
	notYourPlayer(board, isWhite);
	sameSrcAndDst(newX, newY);
	samePlayerPieceAtDst(board, newX, newY);
}