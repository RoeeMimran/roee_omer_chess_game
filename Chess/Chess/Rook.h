#pragma once

#include "GamePiece.h"
#include "Board.h"

class Board;
class GamePiece;

class Rook : public GamePiece//����
{
public:
	Rook(const int x, const int y, const char id);
	void checkMove(Board& board, const int newX, const int newY, bool isWhite);
};

void checkRook(Board& board, int currentX, int currentY, int newX, int newY);
