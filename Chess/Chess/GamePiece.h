#pragma once

#include <iostream>
#include "Exceptions.h"

class Board;

class GamePiece
{
protected:
	int _x;
	int _y;
	char _id;

public:
	// c'tor
	GamePiece(int x, int y, char id);

	// d'tor
	~GamePiece();

	// getters
	int getX() const;
	int getY() const;
	char getId() const;

	// setters
	void setX(const int x);
	void setY(const int y);
	void setId(const char id);

	// methods
	void move(Board& board, const int newX, const int newY, bool isWhite);
	void virtual checkMove(Board& board, const int newX, const int newY, bool isWhite) = 0;
	void sameSrcAndDst(int newX, int newY);
	void notYourPlayer(const Board& board, bool isWhite);
	void samePlayerPieceAtDst(const Board& board, int newX, int newY);
	void virtual chessWillOccure(GamePiece* temp, Board& board, int kingX, int kingY, bool isWhite);
	void checkForSelfChess(Board &board, bool isWhite);
};

int getKingLocation(Board& board, bool isX, bool isWhite);