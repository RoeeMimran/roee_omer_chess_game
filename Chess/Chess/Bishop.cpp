#include "Bishop.h"

Bishops::Bishops(int x, int y, char id) :GamePiece(x, y, id)
{
}

/*
function check if the bishop can move to the wanted place
input: current board, the new place and the current turn
output: none
*/
void Bishops::checkMove(Board& board, const int newX, const int newY, bool isWhite)
{
	checkBishop(board, this->getX(), this->getY(), newX, newY);
	sameSrcAndDst(newX, newY);
	notYourPlayer(board, isWhite);
	samePlayerPieceAtDst(board, newX, newY);
}

/*
function check if the bishop can move there, so we will be able to use the same
function in the queen
function check all the 4 directions he can move to
*/
void checkBishop(Board& board, int currentX, int currentY, int newX, int newY)
{
	int i = 0;
	int tempx = 0;
	int tempy = 0;
	if (!((newX - currentX == newY - currentY) ||
		(currentX - newX == newY - currentY) ||
		(currentX - newX == currentY - newY) ||
		(currentY - newY == newX - currentX)))
		//all the good places he can move to
	{
		throw IllegalMovement();//if not good illegel movement
	}
	if (newX - currentX == newY - currentY)
	{
		tempx = currentX + 1;
		for (i = currentY + 1; i < newY; i++)
		{
			if (board._currBoard[i][tempx])
			{
				throw IllegalMovement();
			}
			tempx += 1;
		}
	}
	if (currentX - newX == newY - currentY)
	{
		tempx = currentX - 1;
		for (i = currentY + 1; i < newY; i++)
		{
			if (board._currBoard[i][tempx])
			{
				throw IllegalMovement();
			}
			tempx -= 1;
		}
	}
	if (currentX - newX == currentY - newY)
	{
		tempx = currentX - 1;
		for (i = currentY - 1; i > newY; i--)
		{
			if (board._currBoard[i][tempx])
			{
				throw IllegalMovement();
			}
			tempx -= 1;
		}
	}
	if (currentY - newY == newX - currentX)
	{
		tempy = currentY - 1;
		for (i = currentX + 1; i < newX; i++)
		{
			if (board._currBoard[tempy][i])
			{
				throw IllegalMovement();
			}
			tempy -= 1;
		}
	}
}