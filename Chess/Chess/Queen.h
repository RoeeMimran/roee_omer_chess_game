#pragma once

#include "Bishop.h"
#include "Rook.h"

class Board;
class GamePiece;

class Queen : public GamePiece//����
{
public:
	Queen(const int x, const int y, const char id);
	void checkMove(Board& board, const int newX, const int newY, bool isWhite);
};
