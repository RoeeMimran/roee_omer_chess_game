#pragma once
#include "Exceptions.h"

class IllegalMovement : public std::exception
{
public:
	const char* what() const;
};
