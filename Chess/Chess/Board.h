#pragma once

#include "Bishop.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"

#define NUM_OF_ROW 8
#define NUM_OF_COL 8

#define START_OF_GAME "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"

#define WHITE_KING 'k'
#define WHITE_QUEEN 'q'
#define WHITE_BISHOP 'b'
#define WHITE_ROOK 'r'
#define WHITE_PAWN 'p'
#define WHITE_KNIGHT 'n'

#define BLACK_KING 'K'
#define BLACK_QUEEN 'Q'
#define BLACK_BISHOP 'B'
#define BLACK_ROOK 'R'
#define BLACK_PAWN 'P'
#define BLACK_KNIGHT 'N'

class Board
{
public:
	// fields
	GamePiece* _currBoard[NUM_OF_COL][NUM_OF_ROW];

	// methods
	Board();
	~Board();
	void printBoard() const;
	void updateBoard(const int oldX, const int oldY, const int newX, const int newY);
	GamePiece* createPiece(char pieceToCreate, const int x, const int y);
};