#pragma once
#include "Exceptions.h"

class ChessWillOccure : public std::exception
{
public:
	const char* what() const;
};
