#pragma once

#include <exception>
#include <iostream>

// include all the expetions so we will be able to include only expetions.h
#include "ChessWillOccure.h"
#include "BoundsError.h"
#include "DestinationError.h"
#include "GameOver.h"
#include "IllegalMovement.h"
#include "NotYourPlayer.h"
#include "SameSrcAndDst.h"
#include "YouMadeChess.h"

// define for each exeption
#define CHESS_WILL_OCCURE "1"
#define NOT_YOUR_PLAYER "2"
#define DESTINATION_ERROR "3"
#define YOU_MADE_CHESS "4"
#define BOUNDS_ERROR "5"
#define ILLEAGAL_MOVEMENT "6"
#define SAME_SRC_AND_DST "7"
#define GAME_OVER "8"