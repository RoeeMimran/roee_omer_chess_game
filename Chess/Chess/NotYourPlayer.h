#pragma once
#include "Exceptions.h"

class NotYourPlayer : public std::exception
{
public:
	const char* what() const;
};