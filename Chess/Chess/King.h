#pragma once
#include "GamePiece.h"
#include "Board.h"

class Board;
class GamePiece;

class King : public GamePiece//���
{
public:
	King(const int x, const int y, const char id);
	~King();
	void checkMove(Board& board, const int newX, const int newY, bool isWhite);
};
