#pragma once

#include "Pipe.h"
#include <iostream>

class Communication
{
public:
	static void connect(Pipe& pipe);
	static std::string getData(Pipe& pipe);
	static void sendData(const char* msg, Pipe& pipe);
	static int dataToIndex(char data);
};
