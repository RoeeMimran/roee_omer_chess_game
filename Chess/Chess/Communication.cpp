#include "Communication.h"

/*
function connect to the GUI with the pipe
and when fail, asking the user to try again or exit
input: reference of the pipe to connect
output: none
*/
void Communication::connect(Pipe& pipe)
{
	bool isConnect = pipe.connect();
	char keepAsking = 'y';

	while (!isConnect && keepAsking != '0')
	{
		std::cout << "Cannot connect to GUI" << std::endl;
		std::cout << "Enter 0 to try to exit the game" << std::endl;
		std::cout << "Enter something else try again" << std::endl;
		std::cin >> keepAsking;

		if (keepAsking != '0')
		{
			isConnect = pipe.connect();
		}
		else
		{
			pipe.close();
			throw Communication();
		}
	}
}

/*
function get the data that the GUI send
input: the pipe
output: the massage the GUI sent us
*/
std::string Communication::getData(Pipe& pipe)
{
	return pipe.getMessageFromGraphics();
}

/*
function send massage to the GUI with the pipe
input: string, the massage to send to the GUI and the pipe
output: none
*/
void Communication::sendData(const char* msg, Pipe& pipe)
{
	char msgToGraphics[1024];
	strcpy_s(msgToGraphics, msg);
	pipe.sendMessageToGraphics(msgToGraphics);
}

/*
function convert the data from the GUI to index of the array
input: the data from GUI
output: the converted data
*/
int Communication::dataToIndex(char data)
{
	int index = 0;
	if (data <= '9')
	{
		index = data - '1';
	}
	else
	{
		index = data - 'a';
	}
	return index;
}
