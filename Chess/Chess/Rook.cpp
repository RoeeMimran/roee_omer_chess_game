#include "Rook.h"
Rook::Rook(const int x, const  int y, const char id) :GamePiece(x, y, id)
{

}

/*
function check the move of the rook
input: current board, the wanted place, and the current place
output: none
*/
void Rook::checkMove(Board& board, const int newX, const int newY, bool isWhite)
{
	checkRook(board, this->getX(), this->getY(), newX, newY);
	notYourPlayer(board, isWhite);
	sameSrcAndDst(newX, newY);
	samePlayerPieceAtDst(board, newX, newY);
}

/*
function check the rook moves, we will be able to use this function in the queen
input: current board, the wanted place, and the current place
output: none
*/
void checkRook(Board& board, int currentX, int currentY, int newX, int newY)
{
	{
		int i = 0;
		if (newX != currentX && newY != currentY)
		{
			throw IllegalMovement();
		}
		if (newX < 0 || newX>8 || newY < 0 || newY >8)
		{
			throw BoundsError();
		}
		if (currentX < newX)
		{
			for (i = currentX + 1; i < newX; i++)//running on all the places between the place before and the new one
			{
				if (board._currBoard[currentY][i])//if someone on their way
				{
					throw IllegalMovement();//if get blocked by people
				}
			}
		}
		else
		{
			for (i = currentX - 1; i > newX; i--)//running on all the places between the place before and the new one
			{
				if (board._currBoard[currentY][i])//if someone on their way
				{
					throw IllegalMovement();//if get blocked by people
				}
			}
		}
		if (currentY < newY)
		{
			for (i = currentY + 1; i < newY; i++)//running on all the places between the place beforeand the new one
			{
				if (board._currBoard[i][currentX])//if piece on there way
				{
					throw IllegalMovement();//if got blocked by people
				}
			}
		}
		else
		{
			for (i = currentY - 1; i > newY; i--)//running on all the places between the place beforeand the new one
			{
				if (board._currBoard[i][currentX])//if piece on there way
				{
					throw IllegalMovement();//if got blocked by people
				}
			}
		}
	}
}
