#include "GamePiece.h"
#include "Board.h"

GamePiece::GamePiece(int x, int y, char id)
{
	this->_x = x;
	this->_y = y;
	this->_id = id;
}

GamePiece::~GamePiece()
{
}

void GamePiece::move(Board& board, const int newX, const int newY, bool isWhite)
{
	int oldX = this->_x;
	int oldY = this->_y;

	checkMove(board, newX, newY, isWhite);//need to check if origin has a piece on it
	board.updateBoard(oldX, oldY, newX, newY);//updating the board

	this->setX(newX);//updating piece x
	this->setY(newY);//updating piece y

	GamePiece* temp = board.createPiece(this->getId(), newX, newY);
	chessWillOccure(temp, board, getKingLocation(board, true, !isWhite), getKingLocation(board, false, !isWhite), isWhite);

	try
	{
		checkForSelfChess(board, isWhite);
	}
	catch (...)
	{
		// catch only chessWillOccure
		board.updateBoard(newX, newY, oldX, oldY);//updating the board

		this->setX(oldX);//updating the old piece x
		this->setY(oldY);//updating the old piece y
		throw YouMadeChess();
	}
	
}
/*
output int
input Board& board,bool isX,bool isWhite
the function will return x or y of kings location depends on (bool isX)
*/
int getKingLocation(Board& board, bool isX, bool isWhite)
{
	int col = 0;
	int row = 0;
	char wantedId = BLACK_KING;

	if (isWhite)//if white piece wanted
	{
		wantedId = WHITE_KING;
	}

	for (col = 0; col < NUM_OF_COL; col++)//running on all y
	{
		for (row = 0; row < NUM_OF_ROW; row++)//running on all x
		{
			if (board._currBoard[col][row] && board._currBoard[col][row]->getId() == wantedId)//if there is a piece and the piece id equal to what we are looking for
			{
				if (isX)//checking what the user wanted
				{
					return row;
				}
				else
				{
					return col;
				}
			}
		}
	}
}

int GamePiece::getX() const
{
	return this->_x;
}

int GamePiece::getY() const
{
	return this->_y;
}

char GamePiece::getId() const
{
	return this->_id;
}

void GamePiece::setX(const int x)
{
	this->_x = x;
}

void GamePiece::setY(const int y)
{
	this->_y = y;
}

void GamePiece::setId(const char id)
{
	this->_id = id;
}
/*
input int newX int newY
output none
the function will check if src and dstare equal
*/
void GamePiece::sameSrcAndDst(int newX, int newY)
{
	if (this->getX() == newX && this->getY() == newY)//if current x,y equal to dst x,y
	{
		throw SameSrcAndDst();
	}
}
/*
input Board& board,bool is whiye
output none
the function will check if u try to move player that is not yours
*/
void GamePiece::notYourPlayer(const Board& board, bool isWhite)
{
	if (board._currBoard[this->getY()][this->getX()])//if there is a piece on the origin place u want to move from
	{
		if (isWhite)//if white
		{
			if (board._currBoard[this->getY()][this->getX()]->getId() >= 'A' && board._currBoard[this->getY()][this->getX()]->getId() <= 'Z')//if the id belong to the same player
			{
				throw NotYourPlayer();
			}
		}
		else
		{
			if (board._currBoard[this->getY()][this->getX()]->getId() >= 'a' && board._currBoard[this->getY()][this->getX()]->getId() <= 'z')//if the id belong to the same player
			{
				throw NotYourPlayer();
			}
		}
	}
}
/*
output none
input Board& board,int newX,int newY
the function will check if the player is trying to eat his own piece
*/
void GamePiece::samePlayerPieceAtDst(const Board& board, int newX, int newY)
{
	if (board._currBoard[newY][newX])//if there is a piece on target place
	{
		if (this->getId() >= 'a' && this->getId() <= 'z')//if belong to player
		{
			if (board._currBoard[newY][newX]->getId() >= 'a' && board._currBoard[newY][newX]->getId() <= 'z')//if belong to the same player
			{
				throw DestinationError();
			}
		}
		else
		{
			if (board._currBoard[newY][newX]->getId() >= 'A' && board._currBoard[newY][newX]->getId() <= 'Z')//if belnog to the same player
			{
				throw DestinationError();
			}
		}
	}
}
/*
input GamePiece* temp ,Board& board,int kingX,int kingY,bool isWhite
output none
the function will check if after u moved the player u made a chess on the enemy

*/
void GamePiece::chessWillOccure(GamePiece* temp, Board& board, int kingX, int kingY, bool isWhite)
{
	bool threaten = false;

	try
	{
		// check if the piece can eat the king
		temp->checkMove(board, kingX, kingY, isWhite);
		threaten = true;
	}
	catch (...)
	{
	}
	if (threaten)
	{
		// none throw was being thrown so the piece can eat our king
		throw ChessWillOccure();
	}
}
/*
output none
input Board& board,bool isWhite
the function will check if u try to move your own player and the move will cause a chess it will prevent u from do so
*/
void GamePiece::checkForSelfChess(Board& board, bool isWhite)
{
	int row = 0;
	int col = 0;
	bool threaten = false;
	int x = getKingLocation(board, true, isWhite);//getting x location of king
	int y = getKingLocation(board, false, isWhite);//getting y location of king

	for (col = 0; col < NUM_OF_COL; col++)//running on y
	{
		for (row = 0; row < NUM_OF_ROW; row++)//running on x
		{
			if (board._currBoard[col][row])//if there is a piece on current place
			{
				if ((!isWhite && board._currBoard[col][row]->getId() <= 'z' && board._currBoard[col][row]->getId() >= 'a') ||//if not white and this black try to move to king place
					(isWhite && board._currBoard[col][row]->getId() <= 'Z' && board._currBoard[col][row]->getId() >= 'A'))//if white and this white try to move to king place
				{
					try
					{
						board._currBoard[col][row]->checkMove(board, x, y, !isWhite);//if can move to kings place
						threaten = true;//if can move
					}
					catch (...)
					{
					}

					if (threaten)//if can't move
					{
						throw YouMadeChess();
					}
				}
			}
		}
	}
}
