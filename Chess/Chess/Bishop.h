#pragma once

#include "GamePiece.h"
#include "Board.h"

class Board;
class GamePiece;

class Bishops : public GamePiece//
{
public:
	Bishops(int x, int y, char id);
	void checkMove(Board& board, const int newX, const int newY, bool isWhite);
};

void checkBishop(Board& board, int currentX, int currentY, int newX, int newY);