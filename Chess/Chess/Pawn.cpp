#include "Pawn.h"
Pawn::Pawn(const int x, const int y, const char id) :GamePiece(x, y, id)
{
	this->_firstMove = true;
}
Pawn::~Pawn()
{

}
/*
input: Board& board,int newX,int newY,bool isWhite
output none
the function will check all the avalible moves that the piece can make and other tests like same source and destenation
not your player,chess will accure and same player at destenation
*/
void Pawn::checkMove(Board& board, const int newX, const int newY, bool isWhite)
{
	int direction = UP;

	if (!isWhite)//depend on side
	{
		direction = DOWN;
	}

	if (this->_firstMove)//return later
	{
		if (this->getY() + 2 * direction < newY * direction)//if tried to move more than 2 places in first move
		{
			throw IllegalMovement();
		}
		if (newY == this->getY() + 2 * direction)//if moving 2 places
		{
			if (board._currBoard[newY - direction][this->getX()])//if piece is on the way
			{
				throw IllegalMovement();
			}
		}
	}
	else
	{
		if (this->getY() + direction < newY)//if tried to move
		{
			throw IllegalMovement();
		}
	}
	
	if ((this->getX() - 1 == newX && this->getY() + direction == newY) ||
		(this->getX() + 1 == newX && this->getY() + direction == newY))//oblique line
	{
		if (board._currBoard[newY][newX])// if there is piece on there way
		{
			samePlayerPieceAtDst(board, newX, newY);//checking if not the player piece
		}
		else
		{
			throw IllegalMovement();//if nothing there cant move oblique line
		}
	}
	else
	{
		if (newX != this->getX())//if not oblique line and tried to move on x 
		{
			throw IllegalMovement();
		}
		else
		{
			if (board._currBoard[newY][newX])//if there is a piece where u want to move
			{
				throw IllegalMovement();
			}
		}
	}

	if (newY < 0 || newY * direction < this->getY() * direction)//checking if move as much as need on y for black or white
	{
		throw IllegalMovement();
	}
	if (newY > 8 || newX > 8)//if outside the map
	{
		throw BoundsError();
	}

	notYourPlayer(board, isWhite);
	sameSrcAndDst(newX, newY);

	if (board._currBoard[newY][newX])//if there is a piece where u want to move
	{
		samePlayerPieceAtDst(board, newX, newY);//checking if not the same player piece
	}

	if (_firstMove)
	{
		_firstMove = false;
	}
}

void Pawn::chessWillOccure(GamePiece* temp, Board& board, int kingX, int kingY, bool isWhite)
{
	int direction = UP;
	bool threaten = false;

	if (!isWhite)//depend on side
	{
		direction = DOWN;
	}

	// check if the piece can eat the king
	if ((this->getX() - 1 == kingX && this->getY() + direction == kingY) ||
		(this->getX() + 1 == kingX && this->getY() + direction == kingY))//oblique line
	{
		threaten = true;
	}
	
	delete temp;
	if (threaten)
	{
		// none throw was being thrown so the piece can eat our king
		throw ChessWillOccure();
	}
}