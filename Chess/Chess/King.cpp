#include "King.h"
King::King(const int x, const int y, const char id) :GamePiece(x, y, id)
{

}
King::~King()
{

}
/*
input: Board& board,int newX,int newY,bool isWhite
output none
the function will check all the avalible moves that the piece can make and other tests like same source and destenation
not your player,chess will accure and same player at destenation
*/
void King::checkMove(Board& board, const int newX, const int newY, bool isWhite)
{
	if (!((this->getX() + 1 == newX && this->getY() + 1 == newY) ||
		(this->getX() - 1 == newX && this->getY() + 1 == newY) ||
		(this->getX() == newX && this->getY() + 1 == newY) ||
		(this->getX() - 1 == newX && this->getY() == newY) ||
		(this->getX() + 1 == newX && this->getY() == newY) ||
		(this->getX() + 1 == newX && this->getY() - 1 == newY) ||
		(this->getX() - 1 == newX && this->getY() - 1 == newY) ||
		(this->getX() == newX && this->getY() - 1 == newY)))
	{
		throw IllegalMovement();
	}
	if (newX < 0 || newX >8 || newY < 0 || newY>8)//outside the map
	{
		throw BoundsError();
	}
	sameSrcAndDst(newX, newY);
	notYourPlayer(board, isWhite);
	samePlayerPieceAtDst(board, newX, newY);
}